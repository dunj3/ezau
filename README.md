EVTC (Zipper And) Uploader
==========================

![MIT license](https://img.shields.io/badge/license-MIT-green) ![Rustlang](https://img.shields.io/badge/language-Rust-green)

`ezau` is a tool which watches a directory for new EVTC log files, uploads them to [dps.report](https://dps.report) and posts the link to a Discord or Matrix channel.

Motivation
----------

The zipping functionality of arcdps used to be limited to Windows.
As a result, Linux users ended up with huge unzipped evtc log files.
`ezau` zipped those for you, and optionally also uploaded them to dps.report.

However, with new versions of arcdps, the logs are always zipped and this functionality works even on Linux.
As such, `ezau` is simply a log uploader now (but `eu` is even worse of a name than `ezau`).

`ezau` was inspired by [`evtc-watch`](https://gitlab.com/networkjanitor/evtc-watch/-/blob/master/evtc-watch) from Xyoz, with the difference that the file watching logic is included in the Rust part.
This makes it cross-platform and fixes some bugs that were present in the `inotifywait` version.

Compilation
-----------

`ezau` is compiled with the help of `cargo`, the standard Rust package manager.
Run `cargo build --release` to build the release version in `./target/release`.

You can shrink the executable size by enabling link time optimization, at the cost of compile time.
To do that, add the following to `Cargo.toml`:

```toml
[profile.release]
lto = true
```

Optional Features
-----------------

The Discord and Matrix functions pull some further dependencies, which are not needed for a basic usage.
Therefore, those features are optional and must be enabled by hand if you wish to use either the Discord integration or the Matrix integration!
To do so, simply adjust your cargo command by adding `--features`:

```sh
# Discord
cargo build --release --features "im-discord"
# Matrix
cargo build --release --features "im-matrix"
# Both
cargo build --release --features "im-matrix im-discord"
```

Configuration
-------------

`ezau` is configured via a configuration file in TOML syntax.
By default, `ezau` looks for `ezau.toml` in the current directory.
You can use `-c` to specify a different configuration file.

A full example configuration is provided here or alternatively as `ezau-sample.toml`:

```toml
# Whether logs should be uploaded.
# For legacy reasons, uploading can be disabled, but you probably want to set
# it to true (otherwise, ezau has no purpose).
# (mandatory)
upload = true

# Whether logs should be sorted when posting to Discord/Matrix.
# If disabled, logs will be posted on a first-come-first-posted basis within
# their group.
# If enabled, categories will be sorted and bosses will be grouped.
# (optional)
sort_logs = false

# Where to upload the logs.
# By default, ezau will attempt to upload to https://dps.report/uploadContent,
# but depending on service availability you might want to use a different domain,
# like b.dps.report instead.
# (optional)
dps_report_upload_url = "https://dps.report/uploadContent"

# Whether logs with an unknown boss should be uploaded.
# By default, ezau only uploads logs with bosses known to evtclib.
# (optional)
upload_unknown = false

# Minimum duration (in milliseconds) for logs to be uploaded.
# Set to 0 to upload all logs.
# (optional)
minimum_duration = 0

# How often uploading a log should be retried when it failed.
# Note that this does not include the first try, so a value of 0 means "try it
# once".
# (optional)
retries = 0

# Discord messaging section.
# If this section is missing, ezau will not do Discord notifications for log uploads.
# Mandatory keys in this section are only mandatory if the section is present, as the whole Discord functionality is optional.
# (optional)
[discord]
# Auth token for a bot account.
# (mandatory)
auth_token = "foobar"
# Channel ID of the channel to which the message should be posted.
# (mandatory)
channel_id = 123

# Matrix messaging section.
# Seee Discord notes.
# (optional)
[matrix]
# Matrix homeserver for the user
# (mandatory)
homeserver = "https://awesome-matrix-server.org"
# Matrix username
# (mandatory)
username = "foobar"
# Matrix password
# (mandatory)
password = "secret-foo"
# Device ID
# If not given, a new one will be created - it is advised though to supply it
# after the first connection.
# (optional)
device_id = "ASDFGH"
# Room ID(s) to which the message should be posted
# (mandatory)
room_id = ["!room123456:homeserver.org"]
```

Usage
-----

`ezau watch <dirname>`: Watch the given directory for new log files and upload them, posting a link to Discord/Matrix.

`ezau upload <filename>`: Upload a single log.
Prints the URL to the console, and (if configured) posts it to Discord/Matrix.
Note that this bypasses the `upload`/`upload_unknown` settings.

See `ezau help` and `ezau help <subcommand>` for more information.

systemd
-------

An exemplary systemd unit file is provided in `ezau.service`.
You can use it to let an `ezau` instange be managed by systemd, which provides features such as persistent logging through `journalctl` and automatic restarts after a crash.
To install `ezau` as a user unit, do the following:

* Edit the paths in `ezau.service` according to your system.
* Install `ezau` as a user unit by copying `ezau.service` to `~/.config/systemd/user/`.
* Reload systemd: `systemctl --user daemon-reload`.
* Start `ezau` manually: `systemctl --user start ezau`.
* *alternatively* Let `ezau` start whenever you log in: `systemctl --user enable ezau`.

For more information about systemd user instances, consult your [systemd manual of trust](https://wiki.archlinux.org/index.php/Systemd/User).

License
-------

`ezau` is licensed under the MIT license:

Copyright © 2020 Dunj3

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
