use super::categories::Category;

use std::{cmp::Ordering, sync::OnceLock, str::FromStr};

use evtclib::{Encounter, Log, Outcome};
use itertools::Itertools;
use regex::Regex;

/// A [`LogBag`] is a struct that holds multiple logs in their categories.
///
/// This is similar to hash map mapping a category to a list of logs, but the [`LogBag`] saves its
/// insertion order. The logs are saved as a line, that way we don't have to re-parse or re-upload
/// them and we can just handle arbitrary data.
#[derive(Default, Debug, Clone, PartialEq, Eq, Hash)]
pub struct LogBag {
    data: Vec<(Category, Vec<String>)>,
}

// Conditional compilation makes it hard to really use all the code, so we just allow dead code
// here locally.
#[allow(dead_code)]
impl LogBag {
    /// Construct a new, empty [`LogBag`].
    pub fn new() -> Self {
        LogBag { data: Vec::new() }
    }

    /// Return an iterator over all available categories.
    pub fn categories(&self) -> impl Iterator<Item = Category> + '_ {
        self.data.iter().map(|x| x.0)
    }

    /// Return an iterator over (category, items).
    pub fn iter(&self) -> impl Iterator<Item = (Category, impl Iterator<Item = &str>)> {
        self.data
            .iter()
            .map(|(cat, lines)| (*cat, lines.iter().map(|l| l as &str)))
    }

    /// Insert an item into the given category.
    ///
    /// If the category does not exist yet, it will be appended at the bottom.
    pub fn insert(&mut self, category: Category, line: String) {
        for (cat, lines) in self.data.iter_mut() {
            if *cat == category {
                lines.push(line);
                return;
            }
        }

        // When we reach here, we don't have the category yet, so we gotta insert it.
        self.data.push((category, vec![line]));
    }

    /// Sort the categories and logs.
    pub fn sort(&mut self) {
        // First sort the categories, the Ord impl of `Category` takes care here
        self.data.sort();
        // Then sort the lines within the categories
        for (_, ref mut lines) in self.data.iter_mut() {
            lines.sort_by(|a, b| {
                let (encounter_a, date_a, url_a) = info_from_line(&a);
                let (encounter_b, date_b, url_b) = info_from_line(&b);
                match (encounter_a, encounter_b) {
                    (None, None) => date_a.cmp(&date_b),
                    (None, Some(_)) => Ordering::Greater,
                    (Some(_), None) => Ordering::Less,
                    (Some(encounter_a), Some(encounter_b)) => encounter_a.partial_cmp(&encounter_b)
                        .or_else(|| order_strikes(encounter_a, encounter_b))
                        // at this point, just give us a stable order
                        .unwrap_or((encounter_a as u16).cmp(&(encounter_b as u16)))
                        .then(date_a.cmp(&date_b))
                        .then(url_a.cmp(&url_b)),
                }
            });
        }
    }

    /// Tries to parse the given text as a plain [`LogBag`].
    pub fn parse_plain(input: &str) -> Option<LogBag> {
        input.parse().ok()
    }

    pub fn parse_markdown(input: &str) -> Option<LogBag> {
        let plain = input
            .split('\n')
            .map(|line| line.trim_matches('*'))
            .join("\n");
        LogBag::parse_plain(&plain)
    }

    /// Renders the contents of this [`LogBag`] as plain text.
    ///
    /// The output of this can be fed back into [`LogBag::parse_plain`] to round-trip.
    pub fn render_plain(&self) -> String {
        self.iter()
            .map(|(category, mut lines)| category.to_string() + "\n" + &lines.join("\n"))
            .join("\n\n")
    }

    /// Renders the contents of this [`LogBag`] as HTML.
    ///
    /// Useful for posting to Matrix chats.
    pub fn render_html(&self) -> String {
        self.iter()
            .map(|(category, mut lines)| {
                format!("<b>{}</b><br>\n{}", category, lines.join("<br>\n"))
            })
            .join("<br>\n<br>\n")
    }

    /// Renders the contents of this [`LogBag`] as Markdown.
    ///
    /// Useful for posting to Discord chats.
    pub fn render_markdown(&self) -> String {
        self.iter()
            .map(|(category, mut lines)| format!("**{}**\n{}", category, lines.join("\n")))
            .join("\n\n")
    }
}

impl FromStr for LogBag {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let data = s
            .trim()
            .split("\n\n")
            .map(|chunk| {
                let mut lines = chunk.split('\n');
                let category = lines.next().unwrap();
                (
                    category.parse::<Category>().unwrap_or_default(),
                    lines.map(ToString::to_string).collect::<Vec<_>>(),
                )
            })
            .filter(|(_, lines)| !lines.is_empty())
            .collect();
        Ok(LogBag { data })
    }
}

impl From<Vec<(Category, Vec<String>)>> for LogBag {
    fn from(data: Vec<(Category, Vec<String>)>) -> Self {
        LogBag { data }
    }
}

/// A helper function to return the right emoji for a given log.
pub fn state_emoji(log: &Log) -> &'static str {
    let outcome = log.analyzer().and_then(|a| a.outcome());
    match outcome {
        Some(Outcome::Success) => "✅",
        Some(Outcome::Failure) => "❌",
        None => "❓",
    }
}

// I don't want to pull in something like `chrono` just to represent a local datetime. Therefore,
// we simply use two integers: The date in the format YYYYMMDD (which automatically sorts
// correctly), and the time in HHMMSS (which does as well).
fn info_from_line(line: &str) -> (Option<Encounter>, Option<(u32, u32)>, &str) {
    static RE: OnceLock<Regex> = OnceLock::new();
    let url_re = RE.get_or_init(|| {
        Regex::new("http(s?)://[^ ]+(?P<date>\\d{8})-(?P<time>\\d{6})_(?P<slug>[a-z]+)").unwrap()
    });
    let Some(caps) = url_re.captures(line) else { return (None, None, line); };
    let date = caps
        .name("date")
        .expect("date group must be present")
        .as_str()
        .parse()
        .expect("matched only digits, parsing should succeeed");
    let time = caps
        .name("time")
        .expect("time group must be present")
        .as_str()
        .parse()
        .expect("matched only digits, parsing should succeeed");

    let encounter = match caps.name("slug").expect("slug group must be present").as_str() {
        "vg" => Some(Encounter::ValeGuardian),
        "gors" => Some(Encounter::Gorseval),
        "sab" => Some(Encounter::Sabetha),
        "sloth" => Some(Encounter::Slothasor),
        "trio" => Some(Encounter::BanditTrio),
        "matt" => Some(Encounter::Matthias),
        "kc" => Some(Encounter::KeepConstruct),
        "tc" => Some(Encounter::TwistedCastle),
        "xera" => Some(Encounter::Xera),
        "cairn" => Some(Encounter::Cairn),
        "mo" => Some(Encounter::MursaatOverseer),
        "sam" => Some(Encounter::Samarog),
        "dei" => Some(Encounter::Deimos),
        "sh" => Some(Encounter::SoullessHorror),
        "rr" => Some(Encounter::RiverOfSouls),
        "bk" => Some(Encounter::BrokenKing),
        "se" => Some(Encounter::EaterOfSouls),
        "eyes" => Some(Encounter::StatueOfDarkness),
        "dhuum" => Some(Encounter::VoiceInTheVoid),
        "ca" => Some(Encounter::ConjuredAmalgamate),
        "twins" => Some(Encounter::TwinLargos),
        "qadim" => Some(Encounter::Qadim),
        "adina" => Some(Encounter::CardinalAdina),
        "sabir" => Some(Encounter::CardinalSabir),
        "qpeer" => Some(Encounter::QadimThePeerless),
        // ambiguous, but it doesn't matter because we map them all to
        // Category::SpecialForcesTrainingArea
        "golem" => Some(Encounter::LargeKittyGolem),
        "ai" => Some(Encounter::Ai),
        "skor" => Some(Encounter::Skorvald),
        "arriv" => Some(Encounter::Artsariiv),
        "arkk" => Some(Encounter::Arkk),
        "mama" => Some(Encounter::MAMA),
        "siax" => Some(Encounter::Siax),
        "enso" => Some(Encounter::Ensolyss),
        "ice" => Some(Encounter::IcebroodConstruct),
        "frae" => Some(Encounter::FraenirOfJormag),
        "falln" => Some(Encounter::SuperKodanBrothers),
        "whisp" => Some(Encounter::WhisperOfJormag),
        "bone" => Some(Encounter::Boneskinner),
        "trin" => Some(Encounter::CaptainMaiTrin),
        "ankka" => Some(Encounter::Ankka),
        "li" => Some(Encounter::MinisterLi),
        "void" => Some(Encounter::Dragonvoid),
        _ => None,
    };

    (encounter, Some((date, time)), caps.get(0).unwrap().as_str())
}

fn order_strikes(left: Encounter, right: Encounter) -> Option<Ordering> {
    // Order according to the wiki at https://wiki.guildwars2.com/wiki/Strike_Mission
    let strikes = &[
        Encounter::IcebroodConstruct,
        Encounter::SuperKodanBrothers,
        Encounter::FraenirOfJormag,
        Encounter::Boneskinner,
        Encounter::WhisperOfJormag,
        Encounter::CaptainMaiTrin,
        Encounter::Ankka,
        Encounter::MinisterLi,
        Encounter::Dragonvoid,
    ];
    if let Some(pos_a) = strikes.iter().position(|x| *x == left) {
        if let Some(pos_b) = strikes.iter().position(|x| *x == right) {
            return Some(pos_a.cmp(&pos_b));
        }
    }
    None
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn insert() {
        let mut logbag = LogBag::new();
        logbag.insert(Category::WvW, "line 1".to_string());
        assert_eq!(logbag.categories().count(), 1);
        logbag.insert(Category::WvW, "line 2".to_string());
        assert_eq!(logbag.categories().count(), 1);
        logbag.insert(Category::Strike, "line 1".to_string());
        assert_eq!(logbag.categories().count(), 2);

        assert_eq!(
            logbag.categories().collect::<Vec<_>>(),
            vec![Category::WvW, Category::Strike]
        );
    }

    #[test]
    fn parse_empty() {
        assert_eq!(LogBag::parse_plain(""), Some(LogBag::new()));
    }

    #[test]
    fn parse_single() {
        let mut logbag = LogBag::new();
        logbag.insert(Category::WvW, "line 1".to_string());
        logbag.insert(Category::WvW, "line 2".to_string());

        assert_eq!(
            LogBag::parse_plain(
                "\
World versus World
line 1
line 2"
            ),
            Some(logbag)
        );
    }

    #[test]
    fn parse_multi() {
        let mut logbag = LogBag::new();
        logbag.insert(Category::WvW, "line 1".to_string());
        logbag.insert(Category::WvW, "line 2".to_string());
        logbag.insert(Category::Strike, "line 1".to_string());
        logbag.insert(Category::Strike, "line 2".to_string());

        assert_eq!(
            LogBag::parse_plain(
                "\
World versus World
line 1
line 2

Strike Mission
line 1
line 2"
            ),
            Some(logbag)
        );
    }

    #[test]
    fn parse_markdown() {
        let mut logbag = LogBag::new();
        logbag.insert(Category::WvW, "line 1".to_string());
        logbag.insert(Category::WvW, "line 2".to_string());
        logbag.insert(Category::Strike, "line 1".to_string());
        logbag.insert(Category::Strike, "line 2".to_string());

        assert_eq!(
            LogBag::parse_markdown(
                "\
**World versus World**
line 1
line 2

**Strike Mission**
line 1
line 2"
            ),
            Some(logbag)
        );
    }

    #[test]
    fn render_plain_single() {
        let mut logbag = LogBag::new();
        logbag.insert(Category::Unknown, "line 1".to_string());
        logbag.insert(Category::Unknown, "line 2".to_string());

        assert_eq!(
            logbag.render_plain(),
            "\
Unknown
line 1
line 2"
        );
    }

    #[test]
    fn render_plain_multi() {
        let mut logbag = LogBag::new();
        logbag.insert(Category::WvW, "line 1".to_string());
        logbag.insert(Category::WvW, "line 2".to_string());
        logbag.insert(Category::SpecialForcesTrainingArea, "enil 1".to_string());
        logbag.insert(Category::SpecialForcesTrainingArea, "enil 2".to_string());

        assert_eq!(
            logbag.render_plain(),
            "\
World versus World
line 1
line 2

Special Forces Training Area
enil 1
enil 2"
        );
    }

    #[test]
    fn render_html_single() {
        let mut logbag = LogBag::new();
        logbag.insert(Category::WvW, "line 1".to_string());
        logbag.insert(Category::WvW, "line 2".to_string());

        assert_eq!(
            logbag.render_html(),
            "\
<b>World versus World</b><br>
line 1<br>
line 2"
        );
    }

    #[test]
    fn render_html_multi() {
        let mut logbag = LogBag::new();
        logbag.insert(Category::WvW, "line 1".to_string());
        logbag.insert(Category::WvW, "line 2".to_string());
        logbag.insert(Category::Unknown, "enil 1".to_string());
        logbag.insert(Category::Unknown, "enil 2".to_string());

        assert_eq!(
            logbag.render_html(),
            "\
<b>World versus World</b><br>
line 1<br>
line 2<br>
<br>
<b>Unknown</b><br>
enil 1<br>
enil 2"
        );
    }

    #[test]
    fn render_markdown_single() {
        let mut logbag = LogBag::new();
        logbag.insert(Category::WvW, "line 1".to_string());
        logbag.insert(Category::WvW, "line 2".to_string());

        assert_eq!(
            logbag.render_markdown(),
            "\
**World versus World**
line 1
line 2"
        );
    }

    #[test]
    fn render_markdown_multi() {
        let mut logbag = LogBag::new();
        logbag.insert(Category::Strike, "line 1".to_string());
        logbag.insert(Category::Strike, "line 2".to_string());
        logbag.insert(Category::SpecialForcesTrainingArea, "enil 1".to_string());
        logbag.insert(Category::SpecialForcesTrainingArea, "enil 2".to_string());

        assert_eq!(
            logbag.render_markdown(),
            "\
**Strike Mission**
line 1
line 2

**Special Forces Training Area**
enil 1
enil 2"
        );
    }

    #[test]
    fn test_info_from_line() {
        assert_eq!(
            info_from_line("✅ https://dps.report/O514-20240827-214630_dhuum"),
            (Some(Encounter::VoiceInTheVoid), Some((20240827, 214630)), "https://dps.report/O514-20240827-214630_dhuum")
        );
    }

    #[test]
    fn test_sort_logbag() {
        let mut logbag = LogBag::new();
        logbag.insert(Category::Wing2, String::from("https://dps.report/abcd-20240101-120000_matt"));
        logbag.insert(Category::Wing2, String::from("https://dps.report/abcd-20240101-115500_matt"));
        logbag.insert(Category::Wing2, String::from("https://dps.report/abcd-20240101-130000_sloth"));
        logbag.insert(Category::Wing1, String::from("https://dps.report/abcd-20240101-120000_gors"));
        logbag.insert(Category::Wing1, String::from("https://dps.report/abcd-20240101-120000_vg"));
        logbag.insert(Category::Wing1, String::from("https://dps.report/abcd-20240101-120000_sab"));

        logbag.sort();

        assert_eq!(logbag.data, vec![
            (Category::Wing1, vec![
                String::from("https://dps.report/abcd-20240101-120000_vg"),
                String::from("https://dps.report/abcd-20240101-120000_gors"),
                String::from("https://dps.report/abcd-20240101-120000_sab"),
            ]),
            (Category::Wing2, vec![
                String::from("https://dps.report/abcd-20240101-130000_sloth"),
                String::from("https://dps.report/abcd-20240101-115500_matt"),
                String::from("https://dps.report/abcd-20240101-120000_matt"),
            ]),
        ]);
    }
}
