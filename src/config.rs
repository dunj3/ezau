use std::{fs, path::Path};

use anyhow::Result;
use serde::Deserialize;
use serde_with::{serde_as, OneOrMany};

/// The main configuration.
#[derive(Debug, Clone, Deserialize, Default)]
pub struct Config {
    /// Flag indicating whether logs should be uploaded or not.
    pub upload: bool,
    /// Whether to sort the logs when posting them.
    #[serde(default)]
    pub sort_logs: bool,
    /// Where to upload the logs.
    #[serde(default = "default_dps_report_upload_url")]
    pub dps_report_upload_url: String,
    /// Flag indicating whether logs with an unknown boss should be uploaded.
    #[serde(default)]
    pub upload_unknown: bool,
    /// Minimum log duration in milliseconds for uploads.
    #[serde(default)]
    pub minimum_duration: u64,
    /// How often the upload to dps.report should be retried.
    #[serde(default)]
    pub retries: u32,
    /// Whether ezau should zip non-zipped logs.
    #[serde(default = "default_zip")]
    // We don't use this anymore, but we keep it so old configs can be parsed and we can properly
    // inform the user.
    pub zip: bool,
    /// Option Discord information for bot postings.
    pub discord: Option<Discord>,
    /// Optional Matrix information for bot postings.
    pub matrix: Option<Matrix>,
}

/// Configuration pertaining to the Discord posting.
#[derive(Debug, Clone, Deserialize)]
pub struct Discord {
    /// Auth token for the Discord bot.
    pub auth_token: String,
    /// Channel ID in which logs should be posted.
    pub channel_id: u64,
}

/// Configuration pertaining to the Matrix posting.
#[serde_as]
#[derive(Debug, Clone, Deserialize)]
pub struct Matrix {
    /// Matrix homeserver.
    pub homeserver: String,
    /// Matrix username.
    pub username: String,
    /// Matrix password.
    pub password: String,
    /// Device ID, or None if a new one should be generated.
    pub device_id: Option<String>,
    /// Room ID(s) where the message should be posted to.
    #[serde_as(as = "OneOrMany<_>")]
    pub room_id: Vec<String>,
}

/// Attempt to load the configuration from the given file.
pub fn load<P: AsRef<Path>>(path: P) -> Result<Config> {
    let content = fs::read(path)?;
    Ok(toml::from_slice(&content)?)
}

fn default_zip() -> bool {
    false
}

fn default_dps_report_upload_url() -> String {
    String::from("https://dps.report/uploadContent")
}
